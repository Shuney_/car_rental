@extends('dashboard.body.main')

@section('content')

<header class="page-header page-header-dark bg-secondary pb-10">
    <x-header.main title="User List" icon="fa-solid fa-users" >
        <x-slot name="path">
            <x-header.path title="Add" path="users.create" icon="fa-solid fa-plus me-3" class="btn btn-primary add-list" />
            <x-header.path title="Clear Search" path="users.index" icon="fa-solid fa-trash me-3" class="btn btn-danger add-list" />
        </x-slot>

        <x-slot name="navigation">
            <x-header.navigation title="Dashboard" path="dashboard" last="Users"  />
        </x-slot>
    </x-header.main>   
    <x-notification.alert />
</header>

<div class="container px-2 mt-n10">
    <div class="card mb-4">
        <div class="card-body">
            <div class="row mx-n4">
                <div class="col-lg-12 card-header mt-n4">
                    <form action="{{ route('users.index') }}" method="GET">
                        <div class="d-flex flex-wrap align-items-center justify-content-between">
                            <div class="form-group row align-items-center">
                                <label for="row" class="col-auto">Row:</label>
                                <div class="col-auto">
                                    <select class="form-control" name="row">
                                        <option value="10" @if(request('row') == '10')selected="selected"@endif>10</option>
                                        <option value="25" @if(request('row') == '25')selected="selected"@endif>25</option>
                                        <option value="50" @if(request('row') == '50')selected="selected"@endif>50</option>
                                        <option value="100" @if(request('row') == '100')selected="selected"@endif>100</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row align-items-center justify-content-between">
                                <label class="control-label col-sm-3" for="search">Search:</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" id="search" class="form-control me-1" name="search" placeholder="Search user" value="{{ request('search') }}">
                                        <div class="input-group-append">
                                            <button type="submit" class="input-group-text bg-primary"><i class="fa-solid fa-magnifying-glass font-size-20 text-white"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <hr>

                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Photo</th>
                                    <th scope="col">@sortablelink('name')</th>
                                    <th scope="col">@sortablelink('username')</th>
                                    <th scope="col">@sortablelink('email')</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <th scope="row">{{ (($users->currentPage() * (request('row') ? request('row') : 10)) - (request('row') ? request('row') : 10)) + $loop->iteration  }}</th>
                                    <td>
                                        <div style="width: 50px;">
                                            <img class="img-fluid" style="border-radius: 50%;" src="{{ $user->photo ? asset('storage/profile/'.auth()->user()->photo) : asset('assets/img/illustrations/profiles/profile-1.png') }}">
                                        </div>
                                    </td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @foreach ($user->roles as $role)
                                            @if ($role->name == 'admin')
                                                <div class="btn btn-sm btn-primary">{{ $role->display_name }}</div>
                                            @else
                                                <div class="btn btn-sm btn-success">{{ $role->display_name }}</div>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="{{ route('users.edit', $user->username) }}" class="btn btn-outline-primary btn-sm mx-1"><i class="fas fa-edit"></i></a>
                                            <form action="{{ route('users.destroy', $user->username) }}" method="POST">
                                                @method('delete')
                                                @csrf
                                                <button type="submit" class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure you want to delete this record?')">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
