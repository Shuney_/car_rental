@extends('dashboard.body.main')

@section('specificpagescripts')
<script src="{{ asset('assets/js/img-preview.js') }}"></script>
@endsection

@section('content')
<header class="page-header page-header-dark bg-secondary pb-10">
    <x-header.main title="Edit User" icon="fa-solid fa-users" >
        <x-slot name="navigation">
            <x-header.navigation title="Dashboard" path="dashboard"  />
            <x-header.navigation title="Users" path="users.index" last="Edit"  />
        </x-slot>
    </x-header.main>  
</header> 

<div class="container-xl px-2 mt-n10">
    <form action="{{ route('users.update', $user->username) }}" method="POST" enctype="multipart/form-data" id="user-submit-form">
        @csrf
        @method('put')
        <div class="row">
            {{-- upload photo --}}
            <div class="col-xl-4 h-auto">
                <x-card.main title="Profile Picture" >
                    <x-slot name="body">
                        <x-form.image_upload model="photo" data="{{ $user->photo }}" />
                        <div style="margin-bottom:5em;"></div>
                    </x-slot>
                </x-card.main>
            </div>

            {{-- user detail --}}
            <div class="col-xl-8">
                <x-card.main title="User Detail" >
                    <x-slot name="body">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <x-form.text title="Name" model="name" data="{{  $user->name }}" required />
                            </div>
                            <div class="col-md-12 mb-3">
                                <x-form.text title="Email Address" model="email" data="{{  $user->email }}" required />
                            </div>
                            <div class="col-md-12 mb-3">
                                <x-form.text title="Username" model="username" data="{{  $user->username }}" required />
                            </div>
                            <div class="col-md-12">
                                <button id="user-submit" class="btn btn-primary" type="button">Save</button>
                                <a class="btn btn-danger" href="{{ route('users.index') }}">Cancel</a>
                            </div>
                        </div>
                    </x-slot>
                </x-card.main>

            </div>
        </div>
    </form>

    <form action="{{ route('users.updatePassword', $user->username) }}" method="POST" id="user-submit-password-form">
        @csrf
        @method('put')
        <div class="row">
            <div class="col-xl-12">
                <x-card.main title="Change Password" >
                    <x-slot name="body">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <x-form.password title="Password" model="password" />
                            </div>
                            <div class="col-md-6 mb-3">
                                <x-form.password title="Password Confirmation" model="password_confirmation" />
                            </div>
                            <div class="col-md-12">
                                <button id="user-submit-password" class="btn btn-primary" type="button">Save</button>
                                <a class="btn btn-danger" href="{{ route('users.index') }}">Cancel</a>
                            </div>
                        </div>
                    </x-slot>
                </x-card.main>
            </div>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        $('#user-submit').click(function (e) {
            e.preventDefault();
            Swal.fire({
                title: 'Do you want to save the changes?',
                showCancelButton: true,
                confirmButtonText: 'Save',
                }).then((result) => {
                if (result.isConfirmed) {
                    $('#user-submit-form').submit();
                }else {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });

        $('#user-submit-password').click(function (e) {
            Swal.fire({
                title: 'Do you want to change the password',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change password!'
                }).then((result) => {
                if (result.isConfirmed) {
                    $('#user-submit-password-form').submit();
                }
            })
        })

    </script>
@endpush
