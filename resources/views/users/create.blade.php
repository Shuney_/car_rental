@extends('dashboard.body.main')

@section('specificpagescripts')
    <script src="{{ asset('assets/js/img-preview.js') }}"></script>
@endsection

@section('content')

<header class="page-header page-header-dark bg-secondary pb-10">
    <x-header.main title="Add User" icon="fa-solid fa-users" >
        <x-slot name="navigation">
            <x-header.navigation title="Dashboard" path="dashboard"  />
            <x-header.navigation title="Users" path="users.index" last="Create"  />
        </x-slot>
    </x-header.main>   
</header>

<div class="container-xl px-2 mt-n10">
    <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data" id="user-submit-form">
        @csrf
        <div class="row">
            {{-- upload photo --}}
            <div class="col-xl-4">
                <x-card.main title="Profile Picture" >
                    <x-slot name="body">
                        <x-form.image_upload model="photo" />
                        <div style="margin-bottom:5.5em;"></div>
                    </x-slot>
                </x-card.main >
            </div>

            {{-- user info --}}
            <div class="col-xl-8">
                <x-card.main title="User Details">
                    <x-slot name="body">
                        <div class="row mb-3">
                            <div class="col-md-12 mb-2">
                                <x-form.text title="Name" model="name" required />
                            </div>
                            <div class="col-md-12 mb-2">
                                <x-form.text title="Email Address" model="email" required />
                            </div>
                            <div class="col-md-12 mb-2">
                                <x-form.text title="Username" model="username" required />
                            </div>
                            <div class="col-md-12 mb-3">
                                <x-form.select title="Role" model="role_id" >
                                    <x-slot name="slot">
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}" @if(old('role_id') == $role->id) selected="selected" @endif>{{ $role->display_name }}</option>
                                        @endforeach
                                    </x-slot>
                                </x-form.select>
                            </div>
                            <div class="col-md-6">
                                <x-form.password title="Password" model="password" required />
                            </div>
                            <div class="col-md-6">
                                <x-form.password title="Password Confirmation" model="password_confirmation" required />
                            </div>
                        </div>
                    </x-slot>
                </x-card.main>
            </div>

            {{-- buttons --}}
            <div class="col-xl-12">
                <x-card.main>
                    <x-slot name="body">
                        <button id="user-submit" class="btn btn-primary" type="button">Save</button>
                        <a class="btn btn-danger" href="{{ route('users.index') }}">Cancel</a>
                    </x-slot>
                </x-card.main>
            </div>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        $('#user-submit').click(function (e) {
            e.preventDefault();
            Swal.fire({
                title: 'Do you want to save the changes?',
                showCancelButton: true,
                confirmButtonText: 'Save',
                }).then((result) => {
                if (result.isConfirmed) {
                    $('#user-submit-form').submit();
                }else {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
    </script>
@endpush
