<div class="container-xl px-4 mt-n4">
    @if (session()->has('success'))
        <script>
            Swal.fire('Saved!', ' {{ session('success') }}', 'success')
        </script>
    @endif
    @if (session()->has('success-updated'))
        <script>
            Swal.fire('Updated!', "{{ session('success-updated') }}", 'success')
        </script>
    @endif
    @if (session()->has('success-deleted'))
        <script>
             Swal.fire( 'Deleted!','Your file has been deleted.','success')
        </script>
    @endif
    @if (session()->has('failed-transaction'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                html: "<p><b>Something went wrong!</b></p>" + "{{ session('failed-deleted') }}",
            })
        </script>
    @endif
</div>


{{-- <div class="alert alert-success alert-icon" role="alert">
    <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
    <div class="alert-icon-aside">
        <i class="far fa-flag"></i>
    </div>
    <div class="alert-icon-content">
        {{ session('success') }}
    </div>
</div> --}}