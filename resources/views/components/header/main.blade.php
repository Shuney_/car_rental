@props([
    'title' =>  '',
    'icon' => 'fa-solid fa-users',
    'path' => null,
    'navigation' => null,
])

<div class="container-xl px-4">
    <div class="page-header-content pt-4">
        <div class="row align-items-center justify-content-between">
            <div class="col-auto mt-4">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i class="{{ $icon }}"></i></div> {{ $title }}
                </h1>
            </div>
            <div class="col-auto my-4">
               {{ $path }}
            </div>
        </div>

        <nav class="mt-4 rounded" aria-label="breadcrumb">
            <ol class="breadcrumb px-3 py-2 rounded mb-0">
                {{ $navigation }}
            </ol>
        </nav>
    </div>
</div>    