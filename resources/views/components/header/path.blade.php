@props([
    'title' => '',
    'path' => null,
    'icon' => 'fa-solid fa-plus me-3',
    'class' => 'btn btn-danger add-list"'
])

<a href="{{ route($path) }}" class="{{ $class }}"><i class="{{ $icon }}"></i>{{ $title }}</a>