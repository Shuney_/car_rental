@props([
    'title' => '',
    'path' => null,
    'last' => null,
])

<li class="breadcrumb-item"><a href="{{ route($path) }}">{{ $title }}</a></li>
@if ($last)
    <li class="breadcrumb-item active">{{ $last }}</li>
@endif