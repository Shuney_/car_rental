@props([
    'body' => null,
    'title' => null
])
<div class="card mb-4">
    @if ($title)
        <div class="card-header">{{ $title }}</div>
    @endif
    <div class="card-body">
        {{ $body }}
    </div>
</div>