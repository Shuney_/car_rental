@props([
    'title' => '',
    'model' => null,
    'data' => null,
    'min' => null,
    'max' => null,
    'readonly' => false,
    'positive' => false,
])

<label class="small mb-1" for="{{ $model }}">{{ $title }} </label>
<input class="form-control form-control-solid @error($model) is-invalid @enderror" id="{{ $model }}" name="{{ $model }}" type="number" placeholder="" value="{{ old($model,$data) }}" 
    @if ($min) min="{{ $min }}" @endif  @if ($max) max="{{ $max }}" @endif
    @if ($readonly) readonly @endif 
    @if ($positive) oninput="validity.valid||(value='');" @endif>
@error($model)
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror