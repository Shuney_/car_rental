@props([
    'model' => null,
    'data' => null,
])

<div class="text-center">
    @if ($data)
        <img class="img-account-profile rounded-circle mb-2" src="{{ $data  ? asset('storage/profile/'.$data ) : asset('assets/img/demo/user-placeholder.svg') }}" alt="" id="image-preview" />
    @else
        <img class="img-account-profile rounded-circle mb-2" src="{{ asset('assets/img/demo/user-placeholder.svg') }}" alt="" id="image-preview" />
    @endif
    <div class="small font-italic text-muted mb-2">JPG or PNG no larger than 1 MB</div>
    <input class="form-control form-control-solid mb-2 @error($model) is-invalid @enderror" type="file"  id="image" name="{{ $model }}" accept="image/*" onchange="previewImage();">
    @error($model)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>