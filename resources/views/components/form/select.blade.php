@props([
    'title' => '',
    'model' => null,
])

<label class="small mb-1" for="{{ $model }}">{{ $title }}<span class="text-danger">*</span></label>
<select class="form-select form-control-solid @error($model) is-invalid @enderror" id="{{ $model }}" name="{{ $model }}">
    <option selected="" disabled="">Select a {{ $title }}:</option>
    {{ $slot }}
</select>
@error($model)
<div class="invalid-feedback">
    {{ $message }}
</div>
@enderror