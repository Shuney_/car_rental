@props([
    'title' => 'Date',
    'required' =>false,
    'model' => null,
    'min' => null,
])

<label class="small my-1" for="{{ $model }}">{{ $title }} @if ($required) <span class="text-danger">*</span></label> @endif
<input class="form-control form-control-solid example-date-input @error($model) is-invalid @enderror" name="{{ $model }}" id="date" type="date" value="{{ old($model) }}" 
min="{{ $min }}" >
@error($model)
<div class="invalid-feedback">
    {{ $message }}
</div>
@enderror
