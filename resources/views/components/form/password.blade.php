@props([
    'title' => '',
    'model' => null,
    'data' => null,
    'required' => false,
])

<label class="small mb-1" for="{{ $model }}">{{ $title }} @if($required) <span class="text-danger">*</span>@endif</label>
<input class="form-control form-control-solid @error($model) is-invalid @enderror" id="{{ $model }}" name="{{ $model }}" type="password" placeholder="" value="{{ old($model) }}" />
@error($model)
    <div class="invalid-feedback">
        {{ $message }}
    </div>
@enderror