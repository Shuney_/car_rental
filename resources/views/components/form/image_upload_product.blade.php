@props([
  
    'model' => null,
    'data' => null,
])

<div class="card-body text-center">
    @if ($data)
    <img class="img-account-profile mb-2" src="{{ $data ? asset('storage/products/'.$data) : asset('assets/img/products/default.webp') }}" alt="" id="image-preview" />
    @else
        <img class="img-account-profile mb-2" src="{{ asset('assets/img/products/default.webp') }}" alt="" id="image-preview" />
    @endif
    <div class="small font-italic text-muted mb-2">JPG or PNG no larger than 1 MB</div>
    <input class="form-control form-control-solid mb-2 @error($model) is-invalid @enderror" type="file"  id="image" name="{{ $model }}" accept="image/*" onchange="previewImage();">
    @error($model)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>

