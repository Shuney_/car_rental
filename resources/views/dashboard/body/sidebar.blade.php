
<nav class="sidenav shadow-right sidenav-light">
    <div class="sidenav-menu">
        <div class="nav accordion" id="accordionSidenav">
            <!-- Sidenav Menu Heading (Core)-->
            <div class="sidenav-menu-heading">Core</div>
            <a class="nav-link {{ Request::is('dashboard*') ? 'active' : '' }}" href="{{ route('dashboard') }}">
                <div class="nav-link-icon"><i data-feather="activity"></i></div>
                Dashboard
            </a>

            @role('admin')
                <!-- Sidenav Heading (Settings)-->
                <div class="sidenav-menu-heading">Settings</div>
                <a class="nav-link {{ Request::is('users*') ? 'active' : '' }}" href="{{ route('users.index') }}">
                    <div class="nav-link-icon"><i class="fa-solid fa-users"></i></div>
                    Users
                </a>
                <a class="nav-link {{ Request::is('cars*') ? 'active' : '' }}" href="{{ route('cars.index') }}">
                    <div class="nav-link-icon"><i class="fa-solid fa-car"></i></div>
                    Car
                </a>
            @endrole

        </div>
    </div>

</nav>
