@extends('dashboard.body.main')

@section('content')
<header class="page-header page-header-dark bg-warning pb-10">
    <x-header.main title="Edit Car" icon="fa-solid fa-car" >
        <x-slot name="navigation">
            <x-header.navigation title="Dashboard" path="dashboard" />
            <x-header.navigation title="Cars" path="cars.index" last="Edit"  />
        </x-slot>
    </x-header.main>   
    <x-notification.alert />
</header>

<div class="container-xl px-2 mt-n10">
    <form action="{{ route('cars.update', $car->slug) }}" method="POST" id="car-submit-form">
        @csrf
        @method('put')

        <x-card.main title="Car Detail" >
            <x-slot name="body">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <x-form.text title="Car Name" model="name" data="{{ $car->name }}" required />
                    </div>
                    <div class="col-md-12 mb-3">
                        <x-form.text title="Car Slug (non editable)." model="slug" data="{{ $car->slug }}" readonly />
                    </div>
                    <div class="col-md-12">
                        <button id="car-submit" class="btn btn-primary" type="button">Update</button>
                        <a class="btn btn-danger" href="{{ route('cars.index') }}">Cancel</a>
                    </div>
                </div>
            </x-slot>
        </x-card.main>
    </form>
</div>

<script>
    // Slug Generator
    const title = document.querySelector("#name");
    const slug = document.querySelector("#slug");
    title.addEventListener("keyup", function() {
        let preslug = title.value;
        preslug = preslug.replace(/ /g,"-");
        slug.value = preslug.toLowerCase()+'-car-rent';
    });
</script>
@endsection

@push('js')
    <script>
        $('#car-submit').click(function (e) {
            e.preventDefault();
            Swal.fire({
                title: 'Do you want to save the changes?',
                showCancelButton: true,
                confirmButtonText: 'Save',
                }).then((result) => {
                if (result.isConfirmed) {
                    $('#car-submit-form').submit();
                }else {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
    </script>
@endpush
