@extends('dashboard.body.main')

@section('content')
<header class="page-header page-header-dark bg-warning pb-10">
    <x-header.main title="Car List" icon="fa-solid fa-car" >
        <x-slot name="path">
            <x-header.path title="Add" path="cars.create" icon="fa-solid fa-plus me-3" class="btn btn-primary add-list" />
            <x-header.path title="Clear Search" path="cars.index" icon="fa-solid fa-trash me-3" class="btn btn-danger add-list" />
        </x-slot>

        <x-slot name="navigation">
            <x-header.navigation title="Dashboard" path="dashboard" last="Cars"  />
        </x-slot>
    </x-header.main>   
    <x-notification.alert />
</header>

<!-- BEGIN: Main Page Content -->
<div class="container px-2 mt-n10">
    <div class="card mb-4">
        <div class="card-body">
            <div class="row mx-n4">
                <div class="col-lg-12 card-header mt-n4">
                    <form action="{{ route('cars.index') }}" method="GET">
                        <div class="d-flex flex-wrap align-items-center justify-content-between">
                            <div class="form-group row align-items-center">
                                <label for="row" class="col-auto">Row:</label>
                                <div class="col-auto">
                                    <select class="form-control" name="row">
                                        <option value="10" @if(request('row') == '10')selected="selected"@endif>10</option>
                                        <option value="25" @if(request('row') == '25')selected="selected"@endif>25</option>
                                        <option value="50" @if(request('row') == '50')selected="selected"@endif>50</option>
                                        <option value="100" @if(request('row') == '100')selected="selected"@endif>100</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row align-items-center justify-content-between">
                                <label class="control-label col-sm-3" for="search">Search:</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" id="search" class="form-control me-1" name="search" placeholder="Search car" value="{{ request('search') }}">
                                        <div class="input-group-append">
                                            <button type="submit" class="input-group-text bg-primary"><i class="fa-solid fa-magnifying-glass font-size-20 text-white"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <hr>

                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">@sortablelink('name', 'Car Name')</th>
                                    <th scope="col">@sortablelink('slug', 'Car Slug')</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cars as $key => $car)
                                <tr>
                                    <th scope="row">{{ (($cars->currentPage() * (request('row') ? request('row') : 10)) - (request('row') ? request('row') : 10)) + $loop->iteration  }}</th>
                                    <td>{{ $car->name }}</td>
                                    <td>{{ $car->slug }}</td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="{{ route('cars.edit', $car) }}" class="btn btn-outline-primary btn-sm mx-1"><i class="fas fa-edit"></i></a>
                                            <form action="{{ route('cars.destroy', $car->slug) }}" method="POST" id="delete-car-form-{{ $key }}" >
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-outline-danger btn-sm" type="button" onclick="deleteCar({{ $key }})" >
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                {{ $cars->links() }}
            </div>
        </div>
    </div>
</div>
<!-- END: Main Page Content -->
@endsection

@push('js')
    <script>
        function deleteCar(index)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    $('#delete-car-form-'+index).submit();
                }
            })
        }
    </script>
@endpush