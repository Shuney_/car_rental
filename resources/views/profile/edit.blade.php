@extends('dashboard.body.main')

@section('specificpagescripts')
<script src="{{ asset('assets/js/img-preview.js') }}"></script>
@endsection

@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Account Settings - Profile
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container-xl px-4 mt-4">
    <nav class="nav nav-borders">
        <a class="nav-link active ms-0" href="{{ route('profile.edit') }}">Profile</a>
        <a class="nav-link" href="{{ route('profile.settings') }}">Settings</a>
    </nav>

    <hr class="mt-0 mb-4" />
    <x-notification.alert />

    <form action="{{ route('profile.update') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('patch')
        <div class="row">
            <div class="col-xl-4">
                <x-card.main title="Profile Picture" >
                    <x-slot name="body">
                        <x-form.image_upload model="photo" data="{{ $user->photo }}" />
                        <div style="margin-bottom:1.5em;"></div>
                    </x-slot>
                </x-card.main>
            </div>
            <div class="col-xl-8">
                <x-card.main title="Account Details" >
                    <x-slot name="body">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <x-form.text title="Username" model="username" data="{{  $user->username }}" required />
                            </div>
                            <div class="col-md-12 mb-3">
                                <x-form.text title="Full Name" model="name" data="{{  $user->name }}" required />
                            </div>
                            <div class="mb-3">
                                <x-form.text title="Email Address" model="email" data="{{  $user->email }}" required />
                            </div>
                        </div>
                    </x-slot>
                </x-card.main>
            </div>
            <div class="col-xl-12">
                <x-card.main title="" >
                    <x-slot name="body">
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </x-slot>
                </x-card.main>
            </div>
        </div>
    </form>
</div>
@endsection
