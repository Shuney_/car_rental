@extends('dashboard.body.main')

@section('specificpagescripts')
<script src="{{ asset('assets/js/img-preview.js') }}"></script>
@endsection

@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Account Settings - Settings
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container-xl px-4 mt-4">
    <nav class="nav nav-borders">
        <a class="nav-link ms-0" href="{{ route('profile.edit') }}">Profile</a>
        <a class="nav-link active" href="{{ route('profile.settings') }}">Settings</a>
    </nav>

    <hr class="mt-0 mb-4" />
    <x-notification.alert />

    <div class="row">
        <form action="{{ route('password.update') }}" method="POST">
            @csrf
            @method('put')
            <div class="col-lg-12 mt-4">
                <x-card.main title="Change Password" >
                    <x-slot name="body">
                        <div class="row">
                            <div class="col-xl-12 mb-3">
                                <x-form.password title="Current Password" model="current_password" required />
                            </div>
                            <div class="col-xl-12 mb-3">
                                <x-form.password title="New Password" model="password" required />
                            </div>
                            <div class="col-xl-12 mb-3">
                                <x-form.password title="Confirm Password" model="password_confirmation" required />
                            </div>
                        </div>
                    </x-slot>
                </x-card.main>
            </div>
            <div class="col-xl-12">
                <x-card.main title="" >
                    <x-slot name="body">
                        <div class="col-xl-12">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </x-slot>
                </x-card.main>
            </div>
        </form>
    </div>
</div>
@endsection
