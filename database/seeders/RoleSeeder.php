<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create([
            'name' => 'car_renter',
            'display_name' => 'Car Rentar', 
            'description' => 'Rent Car', 
        ]);

        Role::create([
            'name' => 'car_owner',
            'display_name' => 'Car Owner', 
            'description' => 'Can add car for rental', 
        ]);
        
        Role::create([
            'name' => 'admin',
            'display_name' => 'User Administrator', 
            'description' => 'User is allowed to manage and edit other users', 
        ]);
    }
}
