<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CarController extends Controller
{
    public function index()
    {
        $row = (int) request('row', 10);
        if ($row < 1 || $row > 100) 
            abort(400, 'The per-page parameter must be an integer between 1 and 100.');
        
        $cars = Car::filter(request(['search']))
                ->sortable()
                ->paginate($row)
                ->appends(request()->query());

        return view('cars.index', [
            'cars' => $cars,
        ]);
    }

    public function create()
    {
        return view('cars.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'slug' => 'required',
        ];
        $validatedData = $request->validate($rules);

        try {
            DB::beginTransaction();

            Car::create($validatedData);

            DB::commit();
            return Redirect::route('cars.index')->with('success', 'Unit has been created!');

        } catch (\Throwable $th) {
            DB::rollback();
            return Redirect::route('cars.create')
                ->with('failed-transaction', 'Unit cannot been created');
        }
    }

    public function show(Car $cats)
    {
      abort(404);
    }

    public function edit(Car $car)
    {
        return view('cars.edit', [
            'car' => $car
        ]);
    }

    public function update(Request $request, Car $car)
    {
        $rules = [
                'name' => 'required',
                'slug' => 'required',
            ];
        $validatedData = $request->validate($rules);

        try {
            DB::beginTransaction();

            Car::where('slug', $car->slug)->update($validatedData);

            DB::commit();
            return Redirect::route('cars.index')->with('success-updated', 'Car has been updated!');

        } catch (\Throwable $th) {
            DB::rollback();
            return Redirect::route('cars.edit',$car)
                ->with('failed-transaction', 'Car cannot been updated');
        }
    }


    public function destroy(Car $car)
    {
        try {
            DB::beginTransaction();

            Car::destroy($car->id);

            DB::commit();
            return Redirect::route('cars.index')->with('success-deleted', 'Unit has been deleted!');
        } catch (\Throwable $th) {
            DB::rollback();
            return Redirect::route('cars.index')
                ->with('failed-transaction', 'Unit cannot been deleted because other PRODUCT refer to this UNIT!');
        }
    }
}
