<?php

namespace App\Http\Controllers;

use App\Models\DailyProduction;
use App\Models\Product;
use App\Models\Unit;
use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        $data_total_price = [];
        $data_total_month = [];
        $month = [1,2,3,4,5,6,7,8,9,10,11,12];

        $user = User::find(auth()->user()->id);

        if($user->hasRole('admin')){
            $count_unit = Unit::count();
            $count_product = Product::count();
            $total_sum = DailyProduction::sum('total_price');
            $count_user = User::count();
        
            $date = DailyProduction::select('date_assign')->whereNotNull('total_price')->distinct()->pluck('date_assign')->take(10)->toArray();

            foreach($date as $item)
                array_push($data_total_price,DailyProduction::select('total_price')->where('date_assign',$item)->sum('total_price'));
        
            foreach($month as $item)
                array_push($data_total_month,DailyProduction::select('total_price')->whereMonth('date_assign',$item)->sum('total_price') ?? 0);
        
            return view('dashboard.index',[
                'count_unit' => $count_unit,
                'count_product' => $count_product,
                'total_sum' =>  $total_sum,
                'count_user' =>  $count_user,
                'data_date' => '"'.implode('","', $date).'"',
                'data_total_price' => implode(",", $data_total_price),
                'data_total_price_month' => implode(",",$data_total_month ),
            ]);
        }
        elseif($user->hasRole('worker')){

            $count_product_assign = DailyProduction::where('user_id',auth()->user()->id)->sum('stock_assign');
            $count_product_sell = DailyProduction::where('user_id',auth()->user()->id)->sum('stock_sell');
            $count_product_return = DailyProduction::where('user_id',auth()->user()->id)->sum('stock_return');
            $total_sum = DailyProduction::where('user_id',auth()->user()->id)->sum('total_price');

            $date = DailyProduction::select('date_assign')->where('user_id',auth()->user()->id)
                                    ->whereNotNull('total_price')
                                    ->distinct()->pluck('date_assign')
                                    ->take(10)->toArray();

            foreach($date as $item)
                array_push($data_total_price,DailyProduction::select('total_price')->where('user_id',auth()->user()->id)->where('date_assign',$item)->sum('total_price'));

            foreach($month as $item)
                array_push($data_total_month,DailyProduction::select('total_price')->where('user_id',auth()->user()->id)->whereMonth('date_assign',$item)->sum('total_price') ?? 0);
            
            return view('dashboard.index',[
                'count_product_assign' => $count_product_assign,
                'count_product_sell' => $count_product_sell,
                'count_product_return' => $count_product_return,
                'total_sum' => $total_sum,
                'data_date' => '"'.implode('","', $date).'"',
                'data_total_price' => implode(",", $data_total_price),
                'data_total_price_month' => implode(",",$data_total_month ),
            ]);
        }
    }
}
