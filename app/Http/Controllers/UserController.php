<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function index()
    {
        $row = (int) request('row', 10);

        if ($row < 1 || $row > 100) {
            abort(400, 'The per-page parameter must be an integer between 1 and 100.');
        }

        $users = User::with('roles')
            ->filter(request(['search']))
            ->sortable()
            ->paginate($row)
            ->appends(request()->query());

        return view('users.index', [
            'users' => $users
        ]);
    }

    public function create()
    {
        $roles = Role::all();
        return view('users.create',[
            'roles' => $roles
        ]);
    }

    public function store(Request $request)
    {
        $worker = Role::where('name','worker')->first();
        $rules = [
            'photo' => 'image|file|max:1024',
            'name' => 'required|max:50',
            'email' => 'required|email|max:50|unique:users,email',
            'username' => 'required|min:4|max:25|alpha_dash:ascii|unique:users,username',
            'password' => 'required_with:password_confirmation|min:6',
            'password_confirmation' => 'same:password|min:6',
        ];

        $validatedData = $request->validate($rules);
        $validatedData['password'] = Hash::make($request->password);

        try{
            DB::beginTransaction();

            if ($file = $request->file('photo')) {
                $fileName = hexdec(uniqid()).'.'.$file->getClientOriginalExtension();
                $path = 'public/profile/';
                $file->storeAs($path, $fileName);
                $validatedData['photo'] = $fileName;
            }

            $user = User::create($validatedData);
            $user->addRole($worker);

            DB::commit();
            return Redirect::route('users.index')->with('success', 'New Worker has been created!');
        } catch (\Throwable $th) {
            DB::rollback();
            return Redirect::route('users.create')
                ->with('failed-transaction', 'New Worker cannot been created');
        }
    }

    public function show(User $user)
    {
        //
    }


    public function edit(User $user)
    {
        return view('users.edit', [
            'user' => $user
        ]);
    }

    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required|max:50',
            'photo' => 'image|file|max:1024',
            'email' => 'required|email|max:50|unique:users,email,'.$user->id,
            'username' => 'required|min:4|max:25|alpha_dash:ascii|unique:users,username,'.$user->id,
        ];

        $validatedData = $request->validate($rules);

        try{
            DB::beginTransaction();

            if ($validatedData['email'] != $user->email) 
                $validatedData['email_verified_at'] = null;
            
            if ($file = $request->file('photo')) {
                $fileName = hexdec(uniqid()).'.'.$file->getClientOriginalExtension();
                $path = 'public/profile/';

                if($user->photo)
                    Storage::delete($path . $user->photo);
                
                $file->storeAs($path, $fileName);
                $validatedData['photo'] = $fileName;
            }

            User::where('id', $user->id)->update($validatedData);

            DB::commit();
            return Redirect::route('users.index')->with('success-updated', 'Worker has been updated!');
        } catch (\Throwable $th) {
            DB::rollback();
            return Redirect::route('users.edit',$user)
                ->with('failed-transaction', 'Worker cannot been updated');
        }
    }

    public function updatePassword(Request $request, String $username)
    {

        $validated = $request->validate([
            'password' => 'required_with:password_confirmation|min:6',
            'password_confirmation' => 'same:password|min:6',
        ]);

        try{
            DB::beginTransaction();

            User::where('username', $username)->update([
                'password' => Hash::make($validated['password'])
            ]);

            DB::commit();
            return Redirect::route('users.index')->with('success', 'Worker password has been updated!');
        } catch (\Throwable $th) {
            DB::rollback();
            return Redirect::route('users.index')
                ->with('failed-transaction', 'Worker password cannot been updated');
        }
    }


    public function destroy(User $user)
    {
        try{
            DB::beginTransaction();

            if($user->photo)
                Storage::delete('public/profile/' . $user->photo);

            User::destroy($user->id);

            DB::commit();
            return Redirect::route('users.index')->with('success', 'Worker has been deleted!');
        } catch (\Throwable $th) {
            DB::rollback();
            return Redirect::route('users.index')
                ->with('failed-transaction', 'Worker cannot been deleted');
        }
    }
}
